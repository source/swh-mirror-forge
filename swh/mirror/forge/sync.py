# Copyright (C) 2017-2024 The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import sys
import urllib.parse
from typing import Dict, Optional

import click
import gitlab
import gitlab.v4.objects
import requests
from swh.core.config import load_from_envvar


def format_project_information(
    project: gitlab.v4.objects.Project, github_org_name: str
) -> Dict[str, str | bool | None]:
    """Given information on repository, extract the needed information for
       mirroring.

    Args:
        project: full information on the repository
        org_name: The github organization name

    """

    return {
        "description": (
            project.description.split("\n")[0] if project.description else project.name
        ),
        "url": project.web_url,
        "name": project.path,
        "url_github": "https://github.com/%s/%s.git" % (github_org_name, project.path),
        "default_branch": project.default_branch,
        "archived": project.archived,
    }


class SWHMirrorForge:
    """Class in charge of mirroring GitLab to GitHub."""

    DEFAULT_CONFIG = {
        "gitlab_instance": None,
        "gitlab_namespaces": [
            "swh/devel",
            "swh/infra",
            "swh/infra/ci-cd",
            "swh/infra/puppet",
        ],
        "github": {
            "api_token": None,
            "user": "swhmirror",
            "org": "SoftwareHeritage",
        },
    }

    def __init__(self) -> None:
        self.config = load_from_envvar(self.DEFAULT_CONFIG)
        self.gitlab = gitlab.Gitlab.from_config(self.config.get("gitlab_instance"))
        self.gitlab.auth()
        self.gitlab_namespaces = self.config["gitlab_namespaces"]
        self.github_user = self.config["github"]["user"]
        self.github_org = self.config["github"]["org"]
        self.github_token = self.config["github"]["api_token"]
        self._check()

    def _check(self):
        """Check the needed tokens are set or fail with an explanatory
        message.

        """
        if not self.gitlab:
            raise ValueError(
                """GitLab connection failed. You need to configure the GitLab
                instance in the python-gitlab configuration, and set it as
                `gitlab_instance` in $SWH_CONFIG_FILENAME."""
            )

        if not self.github_token:
            raise ValueError(
                """Install one personal github token in $SWH_CONFIG_FILENAME
    with scope public_repo (https://github.com/settings/tokens).

    You must be associated to https://github.com/%s organization. Once the
    installation is done, you can trigger this script again."""
                % self.github_org
            )

    def create_or_update_repo_on_github(self, repo, update_only=False, dry_run=True):
        """Create or update routine on github.

        Args:

            repo (dict): Dictionary of information on the repository.
            update_only (bool): Only update repository (defaults to False).

        """

        repo_url = f"https://api.github.com/repos/{self.github_org}/{repo['name']}"

        request_headers = {
            "Authorization": "token %s" % self.github_token,
            "Accept": "application/vnd.github+json",
            "X-GitHub-Api-Version": "2022-11-28",
        }

        expected_project_data = {
            "name": repo["name"],
            "description": "GitHub mirror of " + repo["description"],
            "homepage": repo["url"],
            "private": False,
            "has_issues": False,
            "has_wiki": False,
            "has_downloads": True,
            "has_projects": False,
            "default_branch": repo["default_branch"],
            "archived": repo["archived"],
        }

        r = requests.get(repo_url, headers=request_headers)
        if r.ok:
            project_data = r.json()
            if any(
                project_data[key] != value
                for key, value in expected_project_data.items()
            ):
                query_fn = requests.patch
                error_msg_action = "update"
                api_url = repo_url
            else:
                print("Repo up to date: %s" % repo["name"])
                return
        else:
            if not update_only and r.status_code == 404:
                query_fn = requests.post
                error_msg_action = "create"
                api_url = "https://api.github.com/orgs/%s/repos" % self.github_org
            elif update_only:
                raise ValueError(
                    """Tried to update a non-existing repository: %s (%s)"""
                    % (repo["name"], r.status_code)
                )
            else:
                raise ValueError(
                    "Unexpected status code when getting repository info: %s"
                    % r.status_code
                )

        if dry_run:
            print(
                "Would run %s on %s with data %s"
                % (
                    query_fn.__name__.upper(),
                    api_url,
                    expected_project_data,
                )
            )
            return

        r = query_fn(url=api_url, headers=request_headers, json=expected_project_data)

        if not r.ok:
            raise ValueError(
                "Failure to %s the repository '%s' in github. Status: %s (%s)"
                % (error_msg_action, repo["name"], r.status_code, r.json())
            )

    def mirror_repo_to_github(
        self, repo_id, dry_run: bool = True
    ) -> Optional[Dict[str, str]]:
        """Instantiate a mirror from a repository forge to github if it does
        not already exist.

        Args:
            repo_id: repository's path with namespace.
                     This will be used to fetch information on the repository
                     to mirror.

            dry_run: if True, inhibit the mirror creation (no write is
                    done to either github) or the forge.  Otherwise, the
                    default, it creates the mirror to github. Also, a
                    check is done to stop if a mirror uri is already
                    referenced in the forge about github.

        Returns:
            the repository instance whose mirror has been successfully
            mirrored. None if the mirror already exists.

        Raises:
            ValueError if some error occurred during any creation/reading step.
            The detail of the error is in the message.

        """
        project = self.gitlab.projects.get(repo_id)
        repo = format_project_information(project, self.github_org)

        if not repo:
            raise ValueError(
                "Error when trying to retrieve detailed"
                " information on the repository"
            )

        if project.visibility != "public":
            raise ValueError(
                "Repository view policy for %s is not public" % repo["name"]
            )

        # Check existence of mirror already set
        for mirror in project.remote_mirrors.list():
            parsed = urllib.parse.urlparse(mirror.url)
            # Drop (obscured) credentials
            parsed = parsed._replace(netloc=parsed.netloc.rsplit("@", 1)[-1])
            url = urllib.parse.urlunparse(parsed)
            if url == repo["url_github"]:
                print("Repo %s: URL already exists and matches" % repo["name"])
                return None

        # Create repository in github
        self.create_or_update_repo_on_github(repo, update_only=False, dry_run=dry_run)

        parsed_gh_url = urllib.parse.urlparse(repo["url_github"])
        parsed_gh_url = parsed_gh_url._replace(
            netloc=f"{self.github_user}:{self.github_token}@{parsed_gh_url.netloc}"
        )
        url_with_token = urllib.parse.urlunparse(parsed_gh_url)

        if not dry_run:
            mirror = project.remote_mirrors.create(
                {"url": url_with_token, "enabled": True, "keep_divergent_refs": False}
            )
            sync_url = f"{project.remote_mirrors.path}/{mirror.id}/sync"
            self.gitlab.http_post(sync_url)
        else:
            print(
                "Repo %s: would create mirror to %s"
                % (repo["name"], repo["url_github"])
            )

        return repo

    def mirror_repos_to_github(self, dry_run: bool = True):
        """Mirror repositories to github.

        Args:
            dry_run: if True, inhibit the mirror creation (no write is
                    done to either github) or the forge.  Otherwise, the
                    default, it creates the mirror to github. Also, a
                    check is done to stop if a mirror uri is already
                    referenced in the forge about github.

        Returns:
            dict with keys 'mirrored', 'skipped' and 'errors' keys.

        """
        for namespace in self.gitlab_namespaces:
            g = self.gitlab.groups.get(namespace)
            for project in g.projects.list(iterator=True):
                if project.archived:
                    continue
                if project.visibility != "public":
                    continue
                try:
                    if dry_run:
                        print(f"** DRY RUN {project.path_with_namespace} **")

                    repo_detail = self.mirror_repo_to_github(
                        project.path_with_namespace, dry_run
                    )

                    if repo_detail:
                        yield "Repository %s mirrored at %s." % (
                            repo_detail["url"],
                            repo_detail["url_github"],
                        )
                    else:
                        yield (
                            "Mirror already configured for "
                            f"{project.path_with_namespace}, stopping."
                        )
                except Exception as e:
                    yield str(e)

    def update_mirror_info(self, repo_id, dry_run):
        """Given a repository identifier, retrieve information on such
        repository and update github information on that repository.

        """
        project = self.gitlab.projects.get(repo_id)
        repo = format_project_information(project, self.github_org)

        if not repo:
            raise ValueError(
                "Error when trying to retrieve detailed"
                " information on the repository"
            )

        self.create_or_update_repo_on_github(repo, update_only=True, dry_run=dry_run)

        return repo

    def update_mirrors_info(self, dry_run):
        """Given a query name, loop over the repositories returned by such
        query execution and update information on github for those
        repositories.

        """
        for namespace in self.gitlab_namespaces:
            g = self.gitlab.groups.get(namespace)
            for project in g.projects.list(iterator=True):
                if project.visibility != "public":
                    continue
                repo_id = project.path_with_namespace
                try:
                    if dry_run:
                        print(
                            "** DRY RUN - name '%s' ; id '%s' **"
                            % (project.name, repo_id)
                        )

                    repo_detail = self.update_mirror_info(repo_id, dry_run)

                    if repo_detail:
                        yield "Github mirror %s information updated." % (
                            repo_detail["url_github"]
                        )

                except Exception as e:
                    yield str(e)


@click.group()
def cli():
    pass


@cli.command()
@click.option("--dry-run/--no-dry-run", default=True)
@click.argument("repo_id")
def mirror(dry_run, repo_id):
    """Shell interface to instantiate a mirror from a repository forge to
    github. Does nothing if the repository already exists.

    Args:
        repo_id: repository's identifier callsign. This will be
                       used to fetch information on the repository to
                       mirror.

        dry_run: if True, inhibit the mirror creation (no write is
                done to either github) or the forge.  Otherwise, the
                default, it creates the mirror to github. Also, a
                check is done to stop if a mirror uri is already
                referenced in the forge about github.

    """
    mirror_forge = SWHMirrorForge()

    try:
        repo_id = int(repo_id)
    except Exception:
        pass

    msg = ""
    try:
        if dry_run:
            print("** DRY RUN **")

        repo = mirror_forge.mirror_repo_to_github(repo_id, dry_run)

        if repo:
            msg = "Repository %s mirrored at %s." % (repo["url"], repo["url_github"])
        else:
            msg = "Mirror already configured for %s, stopping." % repo_id
    except Exception as e:
        print(e)
        sys.exit(1)
    else:
        print(msg)
        sys.exit(0)


@cli.command()
@click.option(
    "--dry-run/--no-dry-run",
    default=True,
    help="""Do nothing but read and print what would
                      actually happen without the flag.""",
)
def mirrors(dry_run):
    """Shell interface to instantiate mirrors from a repository forge to
    github. This uses the query_name provided to execute said query.
    The resulting repositories is then mirrored to github if not
    already mirrored.

    Args:
        dry_run: if True, inhibit the mirror creation (no write is
                done to either github) or the forge.  Otherwise, the
                default, it creates the mirror to github. Also, a
                check is done to stop if a mirror uri is already
                referenced in the forge about github.

    """
    mirror_forge = SWHMirrorForge()

    if dry_run:
        print("** DRY RUN **")

    for msg in mirror_forge.mirror_repos_to_github(
        dry_run=dry_run,
    ):
        print(msg)


@cli.command()
@click.option("--dry-run/--no-dry-run", default=True)
@click.argument("repo_id")
def update_github_mirror(dry_run, repo_id):
    """Shell interface to instantiate a mirror from a repository forge to
    github. Does nothing if the repository already exists.

    Args:
        repo_id: repository's identifier callsign. This will be
                       used to fetch information on the repository to
                       mirror.

        dry_run: if True, inhibit the mirror creation (no write is
                done to either github) or the forge.  Otherwise, the
                default, it creates the mirror to github. Also, a
                check is done to stop if a mirror uri is already
                referenced in the forge about github.

    """
    mirror_forge = SWHMirrorForge()

    msg = ""
    try:
        if dry_run:
            print("** DRY RUN **")

        repo = mirror_forge.update_mirror_info(repo_id, dry_run)
        msg = "Github mirror information %s updated." % repo["url_github"]
    except Exception as e:
        print(e)
        sys.exit(1)
    else:
        print(msg)
        sys.exit(0)


@cli.command()
@click.option(
    "--dry-run/--no-dry-run",
    default=True,
    help="""Do nothing but read and print what would
                      actually happen without the flag.""",
)
def update_github_mirrors(dry_run):
    """Shell interface to update mirrors information on github. This uses
    the query_name provided to execute said query.  The resulting
    repositories' github information is then updated.

    Args:
        dry_run: if True, inhibit the mirror creation (no write is
                done to either github) or the forge.  Otherwise, the
                default, it creates the mirror to github. Also, a
                check is done to stop if a mirror uri is already
                referenced in the forge about github.

    """
    mirror_forge = SWHMirrorForge()

    if dry_run:
        print("** DRY RUN **")

    for msg in mirror_forge.update_mirrors_info(dry_run=dry_run):
        print(msg)


if __name__ == "__main__":
    cli()
